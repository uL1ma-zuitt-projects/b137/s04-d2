package b137.obrera.s04d2.abstraction;

public class Person implements Actions, SpecialSkills {

    public Person() {}

    // Methods from Action interface
    public void sleep() {
        // Implementation
        System.out.println("Zzz...");
    }

    public void run() {
        // Implementation
        System.out.println("Running...");
    }

    // Methods from SpecialSkills interface

    public void computerProgram() {
        SpecialSkills.super.computerProgram();
    }

    public void driveACar() {
        SpecialSkills.super.driveACar();
    }
}
