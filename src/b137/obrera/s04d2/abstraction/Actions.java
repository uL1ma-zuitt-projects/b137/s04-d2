package b137.obrera.s04d2.abstraction;

public interface Actions {
    public void sleep();
    public void run();
}
