package b137.obrera.s04d2;

import b137.obrera.s04d2.abstraction.Person;
import b137.obrera.s04d2.inheritance.Animal;
import b137.obrera.s04d2.inheritance.Dog;

import javax.swing.text.Style;

public class Main {

    public static void main(String[] args){
        System.out.println("Inheritance, Abstraction, Polymorphism\n");

        Animal firstAnimal = new Animal("Peechy", "white");

        firstAnimal.showDetails();

        Dog firstDog = new Dog("Poochy", "brown", "Aspin");

        firstDog.bark();
        System.out.println(firstDog.getBreed());
        System.out.println(firstDog.getName());

        firstDog.showDetails();

        System.out.println(); // Abstraction

        Person firstPerson = new Person();

        firstPerson.sleep();
        firstPerson.run();
        firstPerson.computerProgram();
        firstPerson.driveACar();
    }
}
